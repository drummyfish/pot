#include <stdint.h>
#include <stdio.h>

extern uint32_t RAM_DATA_START;
extern uint32_t RAM_DATA_END;
extern uint32_t ROM_START;
extern uint32_t ROM_END;
extern uint32_t BSS_START;
extern uint32_t BSS_END;
//extern uint32_t VECTOR_CHECKSUM;

void __interrupt__reset(void);
void __interrupt__nothing(void);

/* Stack will grow towards lower addresses, so stack top will be at RAM end. */
#define STACK_TOP (0x10008000 - 1)

int main();

void __interrupt__reset()
{

#define ADDR(x) (*((uint32_t *) x))

  ADDR(0x40048080) |=        // SYSAHBCLKCTRL, enable clock to various modules
    (1 << 6)  |   // GPIO
    (1 << 26) |   // RAM1
 
 (1 << 16) | // IOCON
 (1 << 30) | // RTC
// (1 << 19) | // GPIO interrupt
 (3 << 26) | // USBRAM
 (1 << 12);  // USART0

 
ADDR(0x40048008) = 0x00000023; // SYSPLLCTRL, enables and configures PLL

#if 1
// this blinks all PEX pins, for testing
ADDR(0xa0002000) = 0xffffffff;
ADDR(0xa0002004) = 0xffffffff;
ADDR(0xa0002008) = 0xffffffff;

uint32_t val = 0xffffffff;
uint8_t set = 1;

while (1)
{
  ADDR(0xa0002000) = val;
  ADDR(0xa0002004) = val;
  ADDR(0xa0002008) = val;

  val = ~val;

  ADDR(0xa0002080) = val;
  ADDR(0xa0002084) = val;
  ADDR(0xa0002088) = val;

  for (uint32_t i = 0; i < 1000000; ++i)
  {
  }
}
#endif 

// no clock configuration should be needed (maunal p. 28)

 
//  ADDR(0x40048070) = 0x03;   // MAINCLKSEL, set main clock to PLL output







  uint32_t *p = &BSS_START;
  
  // zero fill the BSS segment

  while (p < &BSS_END)
  {
    *p = 0;
    p++;
  }

  // copy ROM data to RAM

  p = &ROM_START;
  uint32_t *p2 = &RAM_DATA_START;

  while ((p < &ROM_END) && (p2 < RAM_DATA_END))
  {
    *p2 = *p;
    p++;
    p2++;
  }

  main();
}

void __interrupt__nothing()
{
}

uint32_t (* const vector[]) __attribute__((section("vectorTable")))=
{
  // documented in ARMv6 Architecture Reference

  #define nothing (uint32_t *) __interrupt__nothing

  (uint32_t *) STACK_TOP,          // stack top
  (uint32_t *) __interrupt__reset, // reset handler
  nothing,                         // NMI
  nothing,                         // HardFault
  0,                               // reserved
  0,                               // reserved
  0,                               // reserved
  0,                               /* Here will be a negated checksum of the
                                      previous items as required by LPC for
                                      safety. This will be filled by the make
                                      script.
                                   */
  nothing,                         // SVCall
  0,                               // reserved
  0,                               // reserved
  nothing,                         // PendSV
  nothing                          // SysTick
 
  /* Here would go extrenal interrupts. */
 
  #undef nothing
};

//----------------------------------------

uint8_t a, b, c;

// 0x3fffc

int main()
{
  a = 2;
  b = 3;
  c = a + b;

  while (1)
  {
  }

  return 0;
}
