#!/bin/bash
clear
clear
rm main.o main.elf
arm-none-eabi-gcc -std=gnu99 -c -Wall -Wextra -fno-builtin -funsigned-char -fdata-sections -MMD -mcpu=cortex-m0plus -mthumb -o main.o main.c
arm-none-eabi-ld -Tlink.ld -o main.elf main.o
arm-none-eabi-objcopy -O binary main.elf main.bin
g++ -x c checksum.c -o checksum
./checksum # compute the checksum in the final bin
