#include <stdio.h>
#include <stdint.h>

int main(void)
{
  FILE *file = fopen("main.bin","rb");

  #define BYTES 16

  uint8_t bytes[BYTES];

  fread(bytes,1,BYTES,file);

  fclose(file);

  int32_t checksum = 0;

  for (int i = 0; i < BYTES; i += 4)
  {

    uint32_t v = 
      bytes[i + 0] +
      bytes[i + 1] * 256 +
      bytes[i + 2] * 256 * 256 +
      bytes[i + 3] * 256 * 256 * 256;

    checksum -= v;
  }

  uint32_t checksum2 = checksum;

  for (int i = 0; i < 4; ++i)
  {
    bytes[i] = checksum2 % 256;
    checksum2 /= 256;
  }

  file = fopen("main.bin","r+b");

  fseek(file,7 * 4,SEEK_SET);
  fwrite(bytes,4,1,file);
  fclose(file);

  return 1;
}
